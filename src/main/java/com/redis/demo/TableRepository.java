package com.redis.demo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TableRepository extends JpaRepository<Table, Integer>{
    Table findById(int id);
    Table findByName(String name);
}
