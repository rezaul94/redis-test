package com.redis.demo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EntityRepository extends CrudRepository<Entity, Integer> {
    Entity findByName(String name);
    Entity findById(int id);
}
