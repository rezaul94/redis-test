package com.redis.demo;

import org.springframework.stereotype.Component;

@Component
public class Converter{
    Entity tableToEntity(Table table){
        Entity entity = new Entity();
        entity.setId(table.getId());
        entity.setName(table.getName());
        entity.setDescription(table.getDescription());
        return entity;

    }

    Table entityToTable(Entity entity){
        Table table = new Table();
        table.setId(entity.getId());
        table.setName(entity.getName());
        table.setDescription(entity.getDescription());
        return table;
    }
}