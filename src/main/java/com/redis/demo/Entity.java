package com.redis.demo;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@RedisHash("Entity")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Entity implements Serializable{
    @Id
    private Integer id;

    @Indexed
    private String name;

    @Indexed
    private String description;

}
