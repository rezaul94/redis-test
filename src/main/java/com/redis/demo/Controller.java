package com.redis.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class Controller{
    private final EntityRepository entityRepository;
    private final TableRepository tableRepository;
    private final Converter converter;

    @PostMapping("/entity/add")
    public Entity saveEntity(@RequestBody Entity entity){
        return entityRepository.save(entity);
    }


    @GetMapping("/entity/{name}")
    public Entity allEntity(@PathVariable("name") final String name) {
        return entityRepository.findByName(name);
    }

    @GetMapping("/entities")
    public Object allEntities() {
        return entityRepository.findAll();
    }

    @PostMapping("/table/add")
    public Table addTable(@RequestBody Table table){
        Table data = tableRepository.save(table);

        return data;
    }


    @GetMapping("/table/id/{id}")
    public Table getTableById(@PathVariable("id") final int id){
        Entity entity = entityRepository.findById(id);
        if (entity == null){
            Table table = tableRepository.findById(id);
            entityRepository.save(converter.tableToEntity(table));
            return table;
        } 

        return converter.entityToTable(entity);
    }


    @GetMapping("/table/name/{name}")
    public Table getTableByName(@PathVariable("name") final String name){
        Entity entity = entityRepository.findByName(name);
        if (entity == null){
            Table table = tableRepository.findByName(name);
            entityRepository.save(converter.tableToEntity(table));
            return table;
        } 

        return converter.entityToTable(entity);
    }

}